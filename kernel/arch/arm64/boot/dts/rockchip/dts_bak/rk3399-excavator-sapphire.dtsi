/*
 * Copyright (c) 2016 Fuzhou Rockchip Electronics Co., Ltd
 *
 * This file is dual-licensed: you can use it either under the terms
 * of the GPL or the X11 license, at your option. Note that this dual
 * licensing only applies to this file, and not this project as a
 * whole.
 *
 *  a) This file is free software; you can redistribute it and/or
 *     modify it under the terms of the GNU General Public License as
 *     published by the Free Software Foundation; either version 2 of the
 *     License, or (at your option) any later version.
 *
 *     This file is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 * Or, alternatively,
 *
 *  b) Permission is hereby granted, free of charge, to any person
 *     obtaining a copy of this software and associated documentation
 *     files (the "Software"), to deal in the Software without
 *     restriction, including without limitation the rights to use,
 *     copy, modify, merge, publish, distribute, sublicense, and/or
 *     sell copies of the Software, and to permit persons to whom the
 *     Software is furnished to do so, subject to the following
 *     conditions:
 *
 *     The above copyright notice and this permission notice shall be
 *     included in all copies or substantial portions of the Software.
 *
 *     THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *     EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 *     OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *     NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *     HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 *     WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *     FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *     OTHER DEALINGS IN THE SOFTWARE.
 */

#include "rk3399-sapphire.dtsi"


/ {
	rt5651-sound {
		status = "okay";
		compatible = "simple-audio-card";
		simple-audio-card,format = "i2s";
		simple-audio-card,name = "realtek,rt5651-codec";
		simple-audio-card,mclk-fs = <256>;
		simple-audio-card,widgets =
			"Microphone", "Mic Jack",
			"Headphone", "Headphone Jack";
		simple-audio-card,routing =
			"Mic Jack", "MICBIAS1",
			"IN1P", "Mic Jack",
			"Headphone Jack", "HPOL",
			"Headphone Jack", "HPOR";
		simple-audio-card,cpu {
			sound-dai = <&i2s0>;
		};
		simple-audio-card,codec {
			sound-dai = <&rt5651>;
		};
	};

	dw_hdmi_audio: dw-hdmi-audio {
		status = "disabled";
		compatible = "rockchip,dw-hdmi-audio";
		#sound-dai-cells = <0>;
	};

	hdmi_sound: hdmi-sound {
		//status = "disabled";
		status = "okay";
		compatible = "simple-audio-card";
		simple-audio-card,format = "i2s";
		simple-audio-card,mclk-fs = <256>;
		simple-audio-card,name = "rockchip,hdmi";

		simple-audio-card,cpu {
			sound-dai = <&i2s2>;
		};
		simple-audio-card,codec {
			sound-dai = <&hdmi>;
		};
	};

	spdif-sound {
		status = "okay";
		//status = "disabled";
		compatible = "simple-audio-card";
		simple-audio-card,name = "ROCKCHIP,SPDIF";
		simple-audio-card,cpu {
			sound-dai = <&spdif>;
		};
		simple-audio-card,codec {
			sound-dai = <&spdif_out>;
		};
	};

	spdif_out: spdif-out {
		status = "okay";
		compatible = "linux,spdif-dit";
		#sound-dai-cells = <0>;
	};

	/*
	sdio_pwrseq: sdio-pwrseq {
		compatible = "mmc-pwrseq-simple";
		clocks = <&rk808 1>;
		clock-names = "ext_clock";
		pinctrl-names = "default";
		pinctrl-0 = <&wifi_enable_h>;

		//
		// On the module itself this is one of these (depending
		// on the actual card populated):
		// - SDIO_RESET_L_WL_REG_ON
		// - PDN (power down when low)
		//
		reset-gpios = <&gpio0 10 GPIO_ACTIVE_LOW>; // GPIO0_B2 
	};*/

	wireless-wlan {
		compatible = "wlan-platdata";
		rockchip,grf = <&grf>;
		wifi_chip_type = "ap6356";
		sdio_vref = <1800>;
		WIFI,host_wake_irq = <&gpio0 3 GPIO_ACTIVE_HIGH>; /* GPIO0_a3 */
		status = "okay";
	};

	wireless-bluetooth {
		compatible = "bluetooth-platdata";
		clocks = <&rk808 1>;
		clock-names = "ext_clock";
		//wifi-bt-power-toggle;
		uart_rts_gpios = <&gpio2 19 GPIO_ACTIVE_LOW>; /* GPIO2_C3 */
		pinctrl-names = "default", "rts_gpio";
		pinctrl-0 = <&uart0_rts>;
		pinctrl-1 = <&uart0_gpios>;
		//BT,power_gpio  = <&gpio3 19 GPIO_ACTIVE_HIGH>; /* GPIOx_xx */
		BT,reset_gpio    = <&gpio0 9 GPIO_ACTIVE_HIGH>; /* GPIO0_B1 */
		BT,wake_gpio     = <&gpio2 26 GPIO_ACTIVE_HIGH>; /* GPIO2_D2 */
		BT,wake_host_irq = <&gpio0 4 GPIO_ACTIVE_HIGH>; /* GPIO0_A4 */
		status = "okay";
	};
};
&tsadc {
        /* tshut mode 0:CRU 1:GPIO */
        rockchip,hw-tshut-mode = <1>;
        /* tshut polarity 0:LOW 1:HIGH */
        rockchip,hw-tshut-polarity = <1>;
        status = "okay";
};
//bill no
&dfi {
	status = "okay";
};

&dmc {
	status = "okay";
	center-supply = <&vdd_center>;
	upthreshold = <40>;
	downdifferential = <20>;
	system-status-freq = <
		/*system status         freq(KHz)*/
		SYS_STATUS_NORMAL       800000
		SYS_STATUS_REBOOT       528000
		SYS_STATUS_SUSPEND      200000
		SYS_STATUS_VIDEO_1080P  200000
		SYS_STATUS_VIDEO_4K     600000
		SYS_STATUS_VIDEO_4K_10B 800000
		SYS_STATUS_PERFORMANCE  800000
		SYS_STATUS_BOOST        400000
		SYS_STATUS_DUALVIEW     600000
		SYS_STATUS_ISP          600000
	>;
	vop-bw-dmc-freq = <
	/* min_bw(MB/s) max_bw(MB/s) freq(KHz) */
		0       577      200000
		578     1701     300000
		1702    99999    400000
	>;
	auto-min-freq = <200000>;
};

&spdif {
	status = "okay";
	pinctrl-0 = <&spdif_bus>;
	i2c-scl-rising-time-ns = <450>;
	i2c-scl-falling-time-ns = <15>;
	#sound-dai-cells = <0>;
};

&i2c1 {
	status = "okay";
	i2c-scl-rising-time-ns = <300>;
	i2c-scl-falling-time-ns = <15>;

	rt5651: rt5651@1a {
		status = "okay";
		#sound-dai-cells = <0>;
		//compatible = "rockchip,rt5651";
		compatible = "realtek,rt5651";
		reg = <0x1a>;
		clocks = <&cru SCLK_I2S_8CH_OUT>;
		clock-names = "mclk";
		pinctrl-names = "default";
		pinctrl-0 = <&i2s_8ch_mclk>;
		spk-con-gpio = <&gpio0 11 GPIO_ACTIVE_HIGH>;
		hp-det-gpio = <&gpio4 28 GPIO_ACTIVE_LOW>;
	};
};

&i2c4 {
	status = "okay";
	i2c-scl-rising-time-ns = <600>;
	i2c-scl-falling-time-ns = <20>;

	max17047@36 {
		status = "okay";
                compatible = "maxim,max17047";
                reg = <0x36>;
                charge-detect-gpio = <&gpio1 9 GPIO_ACTIVE_HIGH>;
                battery-full-detect-gpio = <&gpio1 10 GPIO_ACTIVE_HIGH>;
        };
        sensor@1d {
		status = "okay";
                compatible = "gs_mma8452";
		pinctrl-names = "default";
		pinctrl-0 = <&mma8452_irq_gpio>;
                reg = <0x1d>;
                type = <2>;
                irq-gpio = <&gpio1 23 IRQ_TYPE_EDGE_FALLING>;
                irq_enable = <1>;
                poll_delay_ms = <30>;
//                layout = <2>;
                layout = <4>;
        };
	//mpu6500@68 {
	//	status = "okay";
	//	compatible = "invensense,mpu6500";
	//	reg = <0x68>;
	//	irq-gpio = <&gpio1 22 IRQ_TYPE_EDGE_RISING>;
	//	mpu-int_config = <0x10>;
	//	mpu-level_shifter = <0>;
	//	mpu-orientation = <0 1 0 1 0 0 0 0 1>;
	//	orientation-x= <1>;
	//	orientation-y= <0>;
	//	orientation-z= <0>;
	//	mpu-debug = <1>;
	//};
};
/*
&pcie0 {
	ep-gpios = <&gpio2 4 GPIO_ACTIVE_HIGH>;
	num-lanes = <4>;
	pinctrl-names = "default";
	pinctrl-0 = <&pcie_clkreqn>;
};
*/

&uart0 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart0_xfer &uart0_cts>;
	status = "okay";
};

&rockchip_suspend {
	status = "okay";
	rockchip,sleep-debug-en = <1>;
	rockchip,sleep-mode-config = <
		(0
		| RKPM_SLP_ARMPD
		| RKPM_SLP_PERILPPD
		| RKPM_SLP_DDR_RET
		| RKPM_SLP_PLLPD
		| RKPM_SLP_CENTER_PD
		| RKPM_SLP_AP_PWROFF
		)
		>;
	rockchip,wakeup-config = <
		(0
		| RKPM_GPIO_WKUP_EN
		| RKPM_PWM_WKUP_EN
		)
		>;
		rockchip,pwm-regulator-config = <
		(0
		| PWM2_REGULATOR_EN
		)
		>;
		rockchip,power-ctrl =
		<&gpio1 17 GPIO_ACTIVE_HIGH>,
		<&gpio1 14 GPIO_ACTIVE_HIGH>;
};

&pinctrl {
	sdio-pwrseq {
		wifi_enable_h: wifi-enable-h {
			rockchip,pins =
				<0 10 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	wireless-bluetooth {
		uart0_gpios: uart0-gpios {
			rockchip,pins =
				<2 19 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
	mma8452 {
		mma8452_irq_gpio: mma8452-irq-gpio {
			rockchip,pins = <1 23 RK_FUNC_GPIO &pcfg_pull_up>;
		};
	};
};
